const messages = [
    {
        author: 'Tom',
        message: 'This is the first message in our app!'
    },
    {
        author: 'Tom',
        message: 'This is another message!'
    }
];

function sendMessage() {
    const user = document.getElementById('user-name-input').value;
    const msg_elem = document.getElementById('message-input');
    const msg = msg_elem.value;

    if (!user) {
        alert('Please specify a user name!');
        return;
    }

    if (!msg) {
        alert('Please specify a message!');
        return;
    }

    messages.push({ author: user, message: msg });
    displayMessages(true);

    // Clearing message box
    msg_elem.value = '';

}

function displayMessages(messageAdded = false) {
    const container = document.getElementById('message-container');
    container.innerHTML = '';
    for (let i = 0; i < messages.length; i++) {
        const elem = document.createElement('DIV');
        elem.classList.add('message-box');
        const msg = `<b>${messages[i].author}</b> says: ${messages[i].message}`;
        elem.innerHTML = msg;
        container.appendChild(elem);

        // Adding animation to the last message box if it was just added
        if (messageAdded && i == messages.length - 1) {
            elem.classList.add('entering-animation');
        }
    }
}

displayMessages();
